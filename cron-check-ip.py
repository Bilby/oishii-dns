import os, subprocess, pycurl

"""
Gets the Vultr API Key, server DNS and DNS Record as system environment
variables for security.

Prints instructions for how to get and set the relevant environment variable
if it is not already set.
"""
vultr_subdomain = None
vultr_api_key = None
vultr_dns = None
vultr_record = None

try:
    vultr_subdomain = os.environ['V_SUBDOMAIN']
except KeyError:
    print('===============================================================\n'+\
        'Could not find environment variable for \"V_SUBDOMAIN\".\n'+\
        'Please enter a valid environment variable with:\n'+\
        '  export V_SUBDOMAIN=\"satelite\"\n'+\
        '===============================================================')
    exit(1)

try:
    vultr_api_key = os.environ['V_API_KEY']
except KeyError:
    print('===============================================================\n'+\
        'Could not find environment variable for \"V_API_KEY\".\n'+\
        'Please enter a valid environment variable with:\n'+\
        '  export V_API_KEY=\"**************************************\"\n\n'+\
        'To get your API key please visit:\n'+\
        '  https://my.vultr.com/settings/#settingsapi\n'+\
        '===============================================================')
    exit(1)

try:
    vultr_dns = os.environ['V_DNS']
except KeyError:
    print('===============================================================\n'+\
        'Could not find environment variable for \"V_DNS\".\n'+\
        'Please enter a valid environment variable with:\n'+\
        '  export V_DNS=\"example.com\"\n'+\
        '===============================================================')
    exit(1)

try:
    vultr_record = os.environ['V_RECORD']
except KeyError:
    print('===============================================================\n'+\
        'Could not find environment variable for \"V_RECORD\".\n'+\
        'Please enter a valid environment variable from:\n'+\
        '  curl -sH "Authorization: Bearer '+vultr_api_key+'\" '+\
        '\"https://api.vultr.com/v2/domains/'+vultr_dns+'/records\"\n'+\
        '===============================================================')
    exit(1)


"""
Get the current external IP using the Unix dig command.
"""
out = subprocess.Popen([
    'dig', 
    '+short',
    'myip.opendns.com',
    '@resolver1.opendns.com'],
    stdout=subprocess.PIPE
)

ip = out.stdout.read().decode('ascii').strip()

"""
Try to retreive old IP.
"""
old_ip = '0.0.0.0'

try:
    f = open('old_ip', 'r')
    old_ip = f.read().strip()
    f.close()
except FileNotFoundError:
    print('Old IP file not found - creatign a new one.')

print("Old IP: \"%s\", New IP: \"%s\"" % (old_ip, ip))

"""
Compare the old and the new IP's.
"""
if ip != old_ip:
    """
    PyCurl shiz, let's update our IP with Vultr.
    """
    print("IP\'s do not match. Updating with Vultur, Curl output follows:")

    payload = """
        {
            "type": "A",
            "name": "%s",
            "data": "%s",
            "ttl": 3600,
            "priority": -1
        }
        """ % (vultr_subdomain, ip)

    print("Sending payload:\n%s" % payload)
    c = pycurl.Curl()

    c.setopt(c.URL, "https://api.vultr.com/v2/domains/%s/records/%s" % (
        vultr_dns,
        vultr_record))

    c.setopt(c.VERBOSE, True)
    c.setopt(c.CUSTOMREQUEST, 'PATCH')

    c.setopt(c.HTTPHEADER, [
        "Authorization: Bearer %s" % vultr_api_key,
        "Content-Type: application/json"])

    c.setopt(c.POSTFIELDS, payload)
    c.perform()
    c.close()

    """
    Lastly, update the old_ip file.
    We can wrap this in a try catch block if we encounter problems down the
    road.
    """
    print("Curl job completed, updating stored current IP.")

    f = open('old_ip', 'w')
    f.write(ip)
    f.close()
else:
    print("IP\'s are matching - no need to pester Vultr.")

