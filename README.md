# Oishii DNS

This script is basically dynamic DNS that commits an IP change directly to Vultr DNS.

Made for servers pointed to by Vultr DNS with a non-static IP.

Built to run as a Cron job. Run at whatever interval you deem necesary.

## Requirements

A Linux based always-online machine with the following packages:
 * `libcurl4-openssl-dev`
 * `libssl-dev`
 * `python3`
   - `pycurl`

## Installing Requirements

```sh
sudo apt install libcurl4-openssl-dev libssl-dev python3
pip3 install pycurl
```

## Running Script

Run once, and follow the guided instructions.

```sh
python3 cron-check-ip.py
```

Use this to establish what your environment variables should be, or validate the script is working.

## Crontab Configuration

Here is the Crontab config we use, the frequency is very high, adjust for your needs.

```sh
0 */4 * * * V_SUBDOMAIN="<Your subdomain>" \
  V_API_KEY="<Your Vultr API key, keep this a secret, don't commit it to a repo like me>" \
  V_DNS="<Your fully qualified domain name>" \
  V_RECORD="<Your DNS record>" \
  python3 <Path to the script>
```

